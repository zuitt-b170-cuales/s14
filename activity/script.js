let fname = "John";
let lname = "Smith";
let age = "30";

console.log("First Name: " + fname);
console.log("Last Name: " + lname);
console.log("Age: " + age);

/*function hobbies(hobby1, hobby2, hobby3){
	console.log(`Hobbies: [${hobby1}, ${hobby2}, ${hobby3}]`)
};

hobbies("Biking", "Mountain Climbing", "Swimming");

function address(houseNumber, street, city, state){
	console.log(`Work Address: [${houseNumber}, ${street}, ${city}, ${state}]`)
};

address("32", "Washington", "Lincoln", "Nebraska");
*/

console.log("Hobbies:");
console.log(["Biking","Mountain Climbing", "Swimming"]);
console.log("Work Address:");
console.log({houseNumber: "32", street: "Washington", city: "Lincoln", state: "Nebraska"});


function activity(){
	console.log("John Smith is 30 years of age.");
	console.log("This was printed inside of the function");
	console.log(["Biking", "Mountain Climbing", "Swimming"]);
	console.log("This was printed inside of the function");
	console.log({houseNumber: "32", street: "Washington", city: "Lincoln", state: "Nebraska"});
	console.log("The value of isMarried is: true");
};

activity();

function returnFunction(hobby1, hobby2, hobby3){
	return `${hobby1} ${hobby2} ${hobby3}`
}

let hobbyDobby = returnFunction("Food", "Airplanes", "Yummy");

console.log(hobbyDobby);