//JS Function
/*
	Functions are used to create reusable commands/statements that prevents the dev from typing a bunch of codes
*/


function printStar(){
	console.log("*")
};

printStar();
printStar();
printStar();
printStar();

function sayHello(name){
	/*
		functions can also use parameters. these parameters can be defined and a part of the command inside the function. when called, parameters can be replaced with a target value of the developer.
	*/
	console.log("Hello " + name)
};

sayHello("Owen");
sayHello("Joseph");

/*function alertPrint(){
	alert("Hello");
	console.log("Hello");
};

alertPrint();*/

/*function add(){
	firstNum = 1;
	secondNum = 2;
	console.log(firstNum + secondNum);
}

add();*/

//Function that accepts two numbers and prints the sum

function add(x, y){
	let sum = x + y;
	console.log(sum);
};

add(5, 7);

// Three parameters

function printBio(fname, lname, age){
	// console.log("Hello " + fname + lname + age);

	// using template literals
	/*
		declared by the backticks with dollar sign and curly braces with the parameters inside the braces. the displayed data in the console will be the defined parameter instead of the text inside the curly braces
	*/
	// backticks - the symbol on the left side of the 1 in the keyboard
	console.log(`Hello ${fname} ${lname} ${age}`)
};

printBio("Owen", "Cuales", 23);

function  createFullName(fname, mname, lname){
	// return specifies the value to be given back by the function once it is finished executing. the value can be given to a variable. it only gives value, but does not display the, in the console. that's why we also need to log the variable in the console outside the function.
	return `${fname} ${mname} ${lname}`;
};

let fullName = createFullName("Juan", "Dela", "Cruz");

console.log(fullName);