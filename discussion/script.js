// one line comment (ctrl + slash)

/*
	multiline comment (ctrl + shift + slash)
*/

console.log("Hello Batch 170!");

/*
	Javascript - we can see the messages / log in the console

	Browser Consoles are part of the browsers which will allow is to see / log messages, data or information from the programming language

		they are easily accessed in the dev tools

	Statements
		instructions, expressions tthat we add to the programming language to communicate with the computers.

		usually ending with a semicolon (;). However, JS has implemented a way to automatically add semicolons at the end of the line.

	Syntax
		set of rules that describes how statements should be made/constructed

		lines/blocks of codes must follow a certain set of rules for it to work properly

*/

console.log("Owen");

console.log("Buffalo Wings");
console.log("Katsudon");
console.log("Ramen");

let food1 = "Adobo"

console.log(food1);

/*
	Variables are way to store information or data within the JS.

	to create a variable, we first declare the name of the variable with either let/const keyword:

		let nameOfVariable

	Then, we can initialize (definining) the variable with a value or data

		let nameOfVariable = <value>
			** = means assignment variable

*/

console.log("My favorite food is:" + " " + food1);

let food2 = "Kare Kare";

console.log("My favorite food is: " + food1 + " and " + food2);

let food3;

console.log(food3);

food3 = "ChickenJoy";

console.log(food3);

/*
	we can create variables without values, but the variables' content would log undefined.
*/

//const key word

/*
	const keyword will also allow creation of variable. However, with a const keyword, we create constant variable, which means the data saved in these variables will not change, cannot change and should not be changed.
*/

const pi = 3.1416;

console.log(pi);

/*pi = "Pizza";

console.log(pi);*/

/*
	we also cannot create a const variable without intialization or assigning its value.

const gravity;

console.log(gravity);*/

/*
	Mini activity

*/

food1 = "Pringles";
food2 = "Lays";

console.log(food1 + " and " + food2);

const sunriseDirection = "East";
const sunsetDirection = "West";

console.log(sunriseDirection);
console.log(sunsetDirection);

/*
	Guideline in creating a JS Variable

	1. We can create a let variable with the let keyword. let variables can be reassigned but not redeclared.

	2. Creating a variable has two parts: Declaration of the variable name; and initialization of the intitial value of the varialbe through the use of assignment operator (=).

	3. A let variablee can be declared without initialization. However, the value of the variable will be undefined until it is re-assigned with a value.

	4. Not Dfined vs. Undefined. Not Defined error means that the variable is used but not DECLARED. Undefined results from a variable that is used but not INITIALIZED.

	5. We can use const keyword to create variables. Constant variables cannot be declared without initialization. Constant variables cannot be reassigned.

	6. When creating variable names, start with small caps, this is because we are avoiding conflict with JS that is named with capital letters

	7. If the variable would need two words, the naming convention is the use of camelCase. Do not add any space/s for the variables with words as names.
*/

//Data Types


/*
string
	are data types which are alphanumerical text. They could be a name, phrase, or even a sentence. We can create stringdata types with a single quote ('') or with a double quote ("")

	keyword variable = "string data";
*/

console.log("Sample String Data");

/*
numberic data types
	are data types which are numeric. numeric data types are displayed when numbers are not placed in the quotation marks. if there is mathematical operation needed to be done, numeric data type should be used instead of string
*/

console.log(123456789);
console.log("1+1");

let name = "Owen Cuales";
let birthday = "July 08 1998";
let province = "Nueva Ecija";

console.log(name);
console.log(birthday);
console.log(province);

let highscore = 1080780;
let favoriteNumber = 8;
let favoriteElectricFanNumber = 2;

console.log(highscore);
console.log(favoriteNumber);
console.log(favoriteElectricFanNumber);